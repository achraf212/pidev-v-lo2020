/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import com.easyride.Entite.Evenements;
import com.easyride.Service.Serviceevenements;
import com.easyride.Utils.DataBase;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.PrinterJob;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author suare
 */
public class EventController implements Initializable {
private Connection con=null;
    private PreparedStatement ste=null;
    private ResultSet rs=null;
    @FXML
    private TextField tfnom;
    private TextField tfdate;
    int idRow;
     
 @FXML
    private DatePicker tfdateeve;  
     @FXML
    private TextField tfnombre;
    @FXML
    private Button ajouter;
    @FXML
    private TableView<Evenements> table;
    @FXML
    private TableColumn<Evenements,String> fnom;
    @FXML 
    private TableColumn<Evenements,Integer> fnombre;
    @FXML
     private TableColumn<Evenements,String> fdate;
    @FXML
    private TableColumn<Evenements,String> flieux;
    @FXML
    private TableColumn<Evenements,String> fdescreption;
    ObservableList <Evenements> arr= FXCollections.observableArrayList();
    @FXML
    private Button delete;
    @FXML
    private Button update;
    @FXML
    private TextField SearchNewsfx;
    @FXML
    private TextArea tfdescreption;
    @FXML
    private TextField tflieux;
    @FXML
    private TableColumn<Evenements,String> fid;
   
    
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        Evenements e= new Evenements();
         fid.setCellValueFactory(new PropertyValueFactory<>("id"));
        fnom.setCellValueFactory(new PropertyValueFactory<>("nom_evenements"));
        fdate.setCellValueFactory(new PropertyValueFactory<>("dateeve"));
       fnombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        flieux.setCellValueFactory(new PropertyValueFactory<>("lieuxeve"));
       fdescreption.setCellValueFactory(new PropertyValueFactory<>("descreptioneve"));
                        //Serviceevenements ser= new Serviceevenements();
        try { 
            Serviceevenements ser = new Serviceevenements();
            arr=ser.readAll();
        } catch (SQLException ex) {
            Logger.getLogger(EventController.class.getName()).log(Level.SEVERE, null, ex);
        }
        table.setItems((ObservableList<Evenements>)arr);
        
                        
            con = DataBase.getInstance().getConnection();
         searchNews();
          setCellTbale();
       LoadDataFromDB();
      
       
       
     
       table.getSelectionModel().selectedItemProperty().addListener((ob,od,nv)->{
          // System.out.println(((Evenements)nv).getId());
            try { 
       idRow=((Evenements)nv).getId();
       
        
       }catch (Exception ex) {
           System.out.println("hellp"); }
       
       });
       
        
       
    //  setCellValueFromTableToTextFiled();             

    }  
    private void setCellTbale(){
     fid.setCellValueFactory(new PropertyValueFactory<>("id"));
     fnom.setCellValueFactory(new PropertyValueFactory<>("nom_evenements"));
     fnombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
     fdate.setCellValueFactory(new PropertyValueFactory<>("dateeve"));
     flieux.setCellValueFactory(new PropertyValueFactory<>("lieuxeve"));
     fdescreption.setCellValueFactory(new PropertyValueFactory<>("descreptioneve"));
        
    }
   
    
    public void refresh(){
         Evenements e= new Evenements();
        fnom.setCellValueFactory(new PropertyValueFactory<>("nom_evenements"));
        fnombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));

        fdate.setCellValueFactory(new PropertyValueFactory<>("dateeve"));
         flieux.setCellValueFactory(new PropertyValueFactory<>("lieuxeve"));
        fdescreption.setCellValueFactory(new PropertyValueFactory<>("descreptioneve"));
        
        

                        //Serviceevenements ser= new Serviceevenements();
        try { 
            Serviceevenements ser = new Serviceevenements();
            arr=ser.readAll();
        } catch (SQLException ex) {
            Logger.getLogger(EventController.class.getName()).log(Level.SEVERE, null, ex);
        }
        table.setItems((ObservableList<Evenements>)arr);
        
                        
                        
    }
    
    
    @FXML
     private void ajouter(ActionEvent event) {
         System.out.println("hehheheheh");
            String nomE = tfnom.getText();
            String nombreE = tfnombre.getText();
            int nbr=Integer.parseInt(tfnombre.getText());
            String dateE = tfdateeve.getValue().toString();
            String lieuxE = tflieux.getText();
            String descreptionE = tfdescreption.getText();
        
            Serviceevenements sp = new Serviceevenements();
            Evenements p = new Evenements();
            p.setNom_evenements(nomE);
            p.setNombre(nbr);
            p.setDateeve(dateE);
            p.setDescreptioneve(descreptionE);
            p.setLieuxeve(lieuxE);

            
         try {
            sp.ajouter(p);   

            
         } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
    refresh();
     }
   /*   public List<Evenements> afficher(Evenements E) throws SQLException {/*
       ObservableList<Evenements> arr =FXCollections.observableArrayList();
        ste = con.createStatement();
        ResultSet rs = ste.executeQuery("select * from evenements");
        while (rs.next()) {
    arr.add(new Evenements(rs.getInt("nombre participant") ,rs.getString("nom"),rs.getString("date evenements"),rs.getInt("id")));
            System.out.println("helloooooooooooooo");
            Evenements p = new Evenements();
            arr.add(p);
        }
        return arr;
} /
    } */

    @FXML
    private void delete(ActionEvent Event) throws SQLException {
         Evenements a = table.getSelectionModel().getSelectedItem();
        Serviceevenements ser = new Serviceevenements();
        ser.delete(a);
        arr.clear();
        arr.addAll(ser.readAll());
      
     //  E.Integer.parseInt(t_nombreMax.getText());
       // table.getItems().removeAll(table.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void update(ActionEvent event) {
          String nomE = tfnom.getText();
           
            String nombreE = tfnombre.getText();
        int nbr=Integer.parseInt(tfnombre.getText());
            String dateE = tfdateeve.getValue().toString();
            String lieuxE = tflieux.getText();
            String descreptionE = tfdescreption.getText();
         Serviceevenements sp = new Serviceevenements();
             Evenements info1 = new Evenements(nomE,nbr,dateE,lieuxE,descreptionE);
             info1.setId(idRow);
            System.out.println(info1);
            //int max=Integer.parseInt(tfid.getText());
         try {
            sp.update(info1);     
        refresh();
            
         } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }        /*int idd=Integer.parseInt(tfid.getText());

       String nomE = tfnom.getText();
            String dateE = tfdate.getText();
             String nombreE = tfnombre.getText();
        int nbr=Integer.parseInt(tfnombre.getText());

        java.sql.Timestamp tm = java.sql.Timestamp.valueOf(dateE);
        
        try 
        { 
        Serviceevenements srv = new Serviceevenements();
        Evenements info1 = new Evenements(idd,nomE,tm,nbr);
        srv.update(info1);
        
        } 
        catch (SQLException e){ 
    
         }*/
   
  /* private void setCellValueFromTableToTextFiled(){
        table.setOnMouseClicked( e -> {
            Evenements inf1 = table.getItems().get(table.getSelectionModel().getSelectedIndex());
            tfnom.setText(inf1.getNom_evenements());
            tfnombre.setText(""+inf1.getNombre());
            tfdate.setText(inf1.getDateeve());
            tflieux.setText(inf1.getLieuxeve());
            tfdescreption.setText(inf1.getDescreptioneve());
        });
    }
   */
    private void  LoadDataFromDB(){
        arr.clear();
        
        try {
            ste = con.prepareStatement("Select * from evenements");
            rs= ste.executeQuery();
            while(rs.next()){
            arr.add(new Evenements(rs.getInt(1),rs.getString(2), rs.getInt(3),rs.getString(4),rs.getString(5),rs.getString(6)));
            }
            
        } catch (SQLException e) {
        }
        table.setItems(arr);
    }
    private void searchNews(){
        SearchNewsfx.setOnKeyReleased(e->{
            if(SearchNewsfx.getText().equals("")){
                LoadDataFromDB();
            }
            else{
                arr.clear();
                String sql = "SELECT * FROM `evenements` where nom_evenements LIKE '%"+SearchNewsfx.getText()+"%'";
                try {
                    ste = con.prepareStatement(sql);
                    rs = ste.executeQuery();
                    while(rs.next()){
            arr.add(new Evenements(rs.getString(2), rs.getInt(3),rs.getString(4),rs.getString(5),rs.getString(6)));
                        
                    }   
                    table.setItems(arr);
                } catch (SQLException r) {
                }
                
            }
        });
    }
    @FXML
    private void imprimer(ActionEvent event) {
        System.out.println(" can I print?");
        PrinterJob printerJob = PrinterJob.createPrinterJob();
        if (printerJob.printPage(table)) {
            printerJob.endJob();
            System.out.println("printed");
        }
    }
   
    
  
    
}